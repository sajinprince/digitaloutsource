﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.UI.WebControls;

namespace Test7
{
    public partial class frmPlayerDetails : Form
    {
        public frmPlayerDetails()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var pServiceClient = new PlayerListClient.PlayerServiceClient();
            var playerlist=pServiceClient.GetDataUsingDataContract();
            DataGridView GridView = new DataGridView() { Visible = true, DataSource = playerlist,Width=800,Height=600 };
            this.Controls.Add(GridView);
            GridView.AutoResizeColumns();   
        }
    }
}
