﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test4;
using Test4.Models;
using System.Messaging;

namespace Test4.Services
{
    public class Save: Isave
    {
        public void SaveModel(PlayerDetails model, Test4Context db)
        {
            if (model != null)
            {
                db.PlayerDetails.Add(model);
                db.SaveChanges();
            }
        }
        public Message[] SaveModeltoQue(PlayerDetails model)
        {
            var MQ = new MessageQueue();
            if (!MessageQueue.Exists(@".\Private$\MyQueue"))
            {
                MessageQueue.Create(@".\Private$\MyQueue");
            }
            MessageQueue queue = new MessageQueue(@".\Private$\MyQueue");
           
            using ( queue )
            {
                queue.Label = "1";
                queue.Send(model);
                return queue.GetAllMessages();
            }
      
        }
        public List<PlayerDetails> GetPlayers()
        {
            Test4Context db = new Test4Context();
            List<PlayerDetails> playerlist = db.PlayerDetails.ToList();
            return db.PlayerDetails.ToList();
        }
    }
}