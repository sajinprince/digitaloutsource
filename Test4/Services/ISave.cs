﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test4.Models;
using System.Messaging;


namespace Test4.Services
{
    public interface Isave
    {
        void SaveModel(PlayerDetails model, Test4Context db);
        Message[] SaveModeltoQue(PlayerDetails model);
        List<PlayerDetails> GetPlayers();
    }
    public interface IMyInterface
    {
        
    }
}
