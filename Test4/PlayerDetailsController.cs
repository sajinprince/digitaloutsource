﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Test4.Models;

namespace Test4
{
    public class PlayerDetailsController : Controller
    {
        private Test4Context db = new Test4Context();

        // GET: PlayerDetails
        public ActionResult Index()
        {
            return View(db.PlayerDetails.ToList());
        }

        // GET: PlayerDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayerDetails playerDetails = db.PlayerDetails.Find(id);
            if (playerDetails == null)
            {
                return HttpNotFound();
            }
            return View(playerDetails);
        }

        // GET: PlayerDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PlayerDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDNumber,Title,LastName,FirstName,DateofBirth,Email")] PlayerDetails playerDetails)
        {
            if (ModelState.IsValid)
            {
                db.PlayerDetails.Add(playerDetails);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(playerDetails);
        }

        // GET: PlayerDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayerDetails playerDetails = db.PlayerDetails.Find(id);
            if (playerDetails == null)
            {
                return HttpNotFound();
            }
            return View(playerDetails);
        }

        // POST: PlayerDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDNumber,Title,LastName,FirstName,DateofBirth,Email")] PlayerDetails playerDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playerDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playerDetails);
        }

        // GET: PlayerDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayerDetails playerDetails = db.PlayerDetails.Find(id);
            if (playerDetails == null)
            {
                return HttpNotFound();
            }
            return View(playerDetails);
        }

        // POST: PlayerDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PlayerDetails playerDetails = db.PlayerDetails.Find(id);
            db.PlayerDetails.Remove(playerDetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
