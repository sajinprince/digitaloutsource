﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test4.Models
{
    [Table("PlayertDetails")]
    public class PlayerDetails
    {
        [Key] public int IDNumber { get; set; }
        public string Title { get; set; }
        [Display(Name = "Surname")]
        //[Editable(false)]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        public DateTime DateofBirth { get; set; }
        public string Email { get; set; }
        
        /*
        public PlayerDetails()
        {
            DateofBirth = new DateTime();
            IDNumber = new int();
            
        }
        */
    }

    
}
    