﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Test4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //ncl0703 setting it here for now so i can use this controller for the MSMQs
            //Could have set it in the routeconfig to just call Playerdetails
           

            string ControllerName = "PlayerDetails";
            return RedirectToAction("Create", ControllerName);
            //return View("Create");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}