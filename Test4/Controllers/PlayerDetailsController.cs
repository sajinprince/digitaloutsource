﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using Test4.Models;
using Test4.Services;

namespace Test4
{
    //[Dependency]
    //public Isave ; 
    public class PlayerDetailsController : Controller
    {
        //public Isave SaveService; 
        private Test4Context db = new Test4Context();

        // GET: PlayerDetails
        public ActionResult Index()
        {
            return View(db.PlayerDetails.ToList());
        }

        // GET: PlayerDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayerDetails playerDetails = db.PlayerDetails.Find(id);
            if (playerDetails == null)
            {
                return HttpNotFound();
            }
            return View(playerDetails);
        }

        // GET: PlayerDetails/Create
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDNumber,Title,LastName,FirstName,DateofBirth,Email")] PlayerDetails playerDetails)
        {
            if (ModelState.IsValid)
            {
                Save SaveService = new Save();

                Isave mqSave = (Isave)SaveService;
               
               //ncl0703 Saves to Que and retrieves message off the que
               var Message= mqSave.SaveModeltoQue(playerDetails);

                //ncl0703 converts the message back to the PayerDetails class i setup. I see the MQ message uses generics with boxing and unboxing
                //@liberty we work with biztalk not Message ques
                var playerfromq = (PlayerDetails)Message.LastOrDefault().Body;
                //Then saves to the database using entity framework.
                mqSave.SaveModel(playerfromq, db);
                
                /* ncl0703 moved both saves to interface
                db.PlayerDetails.Add(playerDetails);
                db.SaveChanges();
                */
                return RedirectToAction("Index");
            }

            return View(playerDetails);
        }

        // GET: PlayerDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayerDetails playerDetails = db.PlayerDetails.Find(id);
            if (playerDetails == null)
            {
                return HttpNotFound();
            }
            return View(playerDetails);
        }

        // POST: PlayerDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDNumber,Title,LastName,FirstName,DateofBirth,Email")] PlayerDetails playerDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playerDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playerDetails);
        }

        // GET: PlayerDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlayerDetails playerDetails = db.PlayerDetails.Find(id);
            if (playerDetails == null)
            {
                return HttpNotFound();
            }
            return View(playerDetails);
        }

        // POST: PlayerDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PlayerDetails playerDetails = db.PlayerDetails.Find(id);
            db.PlayerDetails.Remove(playerDetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
