﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Test4.Services;
using Test4.Models;

namespace Test6
{
    public class PlayerService : IPlayerService
    {
        
        public List<PlayerDetails> GetDataUsingDataContract()
        {
            //ncl0703 adding referance to models from test 4 so we only need to maintain one model going forward re-use
            //This is pointed to its own self contained DB for project6 in the web.config. Using relative paths
            List<PlayerDetails> composite;
            Save sService = new Save();
            composite = sService.GetPlayers();
            return composite;
        }
    }
}
