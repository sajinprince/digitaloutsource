﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Test4.Models;


namespace Test6
{
    [ServiceContract]
    public interface IPlayerService
    {
       
        [OperationContract]
        List<PlayerDetails> GetDataUsingDataContract();
    }

    [DataContract]
    public class CompositeType 
    {
        public List<PlayerDetails> _playerList;
        
        [DataMember]
        public List<PlayerDetails> playerList
        {
            get { return _playerList; }
            set { _playerList = value; }
        }
    }
}
