﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Test2
{      
        public class UnitTests
        {
            [Test]
            public static void checkpascal()
            {
                //ncl0703 I could have maded a loop to test each element but using the example in the spec
                Assert.AreEqual(Program.pascal(0,0), 1);
                Assert.AreEqual(Program.pascal(1,0), 1);
                Assert.AreEqual(Program.pascal(1,1), 1);
                Assert.AreEqual(Program.pascal(0,2), 1);
                Assert.AreEqual(Program.pascal(2,1), 2);
                Assert.AreEqual(Program.pascal(1,3), 2);
                Assert.AreEqual(Program.pascal(3, 2), 3);
                Assert.AreEqual(Program.pascal(3, 3), 1);
                Assert.AreEqual(Program.pascal(4, 0), 1);
                Assert.AreEqual(Program.pascal(4, 1), 4);
                Assert.AreEqual(Program.pascal(4, 2), 6);
                Assert.AreEqual(Program.pascal(4, 3), 4);
                Assert.AreEqual(Program.pascal(4, 4), 1);
            Console.WriteLine("All unit tests successfull");
                Console.WriteLine("Yay");
            }
        }
    
    public class Program
    {
        public static void Main(string[] args)
        {
            UnitTests.checkpascal();
        }
        public static int pascal(int column, int row)
        {
            int calcvalue = 0;
            if ((column + 1) == 1 || (row + 1) == 1 || column == row)
            {
                return 1;
            }
            else
            {
                 calcvalue=pascal(column - 1, row - 1) + pascal(column - 1, row);
                return calcvalue;
            }
        }

    }
}
