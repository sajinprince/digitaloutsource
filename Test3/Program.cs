﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test3;
using static Test3.Box;

namespace Test3
{
    public class Box
    {
        //ncl0703 not adding interface because applying keep it simple
        public double Width { get; set; }
        public double Height { get; set; }
        public double Radius { get; set; }
        public virtual BoxType BaseBoxType { get;}

        public virtual void Add()
        {
            using (var con = new SqlConnection())
            {
                SqlCommand cmd;
                cmd = new SqlCommand(string.Format("insert into [Table] values({0}, {1}, {2})", BaseBoxType, Width, Height), con);
                cmd.ExecuteNonQuery();
            }
        }
        public virtual double CalculateArea() {
            
            return 0;
        }

        //ncl0703 just so i can test my overrides
        public static void Main(string[] args)
        {
            var test = new Rectangle();
            var testcircle = new Circle();
        }
    }
    public class Rectangle : Box
    {
      public override BoxType BaseBoxType { get { return BoxType.Rectangle; } }

        public override double CalculateArea()
        {
            return Width * Height;
        }
    }
    public class Square : Box
    {
        public override BoxType BaseBoxType { get { return BoxType.Square; } }
        public override double CalculateArea()
            {
                return Width * Height;
            }

        }
    public class Circle : Box
        {
        public override BoxType BaseBoxType { get { return BoxType.Circle; } }
        public override double CalculateArea()
            {
                return Radius * Math.PI;
            }
            public override void Add()
            {
            //ncl003 we can use base.Add but then you need to overide all other methods so this is cleaner. 
            //The other 2 method have the same logic so only overiding the circle
                using (var con = new SqlConnection())
                {
                    SqlCommand cmd;
                    cmd = new SqlCommand(string.Format("insert into [Table] values({0}, {1})", BaseBoxType, Radius), con);
                }
            }
        }
    public enum BoxType { Unknown, Square, Rectangle, Circle }
}



