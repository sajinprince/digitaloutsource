﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace Test1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //creating text file in code so i can give you standalone solution
            string[] strCreateContent= new string[] { "6, 12, 2, 9, 17", "7, 3, 15, 19, 4", "10, 5, 8, 21, 13" };
            string fileName = "numbers.txt";
            if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            File.WriteAllLines(fileName,strCreateContent);
            var ListArray=ReadFile(fileName);

            ListArray.ForEach(Line =>
            {
                WriteSumArraytoFile(Line.ToArray());
                
            });
            
         }

        private static void WriteSumArraytoFile(int[] numbers)
        {
            int sum = 0;
            
            sum = numbers.Sum();
            if (File.Exists(sum.ToString()))
            {
                File.Delete(sum.ToString());
            }
            IEnumerable<string> totsum = new string[] { sum.ToString() };
            File.WriteAllLines(sum.ToString(), totsum);

        }
        private static List<int[]> ReadFile(string fileName)
        {
            List<int[]> Numberlist= new List<int[]>();
            var fContent= File.ReadLines(fileName);
            
            fContent.ToList().ForEach(line =>
            {
                    int[] numbers = line.Split(',').Select(Int32.Parse).ToArray();
                    Numberlist.Add(numbers);
            });
            var sortedlist=Numberlist.OrderBy(c => c.Sum()).ToList() ;            
            return sortedlist;

        }

    }
    
  
}
        
       
    

